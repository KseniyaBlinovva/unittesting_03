package edu.epam.izhevsk.junit;

import org.junit.Before;

import static org.mockito.AdditionalMatchers.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PaymentControllerTest {
    @Mock
    AccountService mockAccount;
    @Mock
    DepositService mockDepositeServise;
    @InjectMocks
    PaymentController controller;

   @Before
    public void setup() throws InsufficientFundsException{
       when(mockAccount.isUserAuthenticated(100L)).thenReturn(true);
       when(mockDepositeServise.deposit(lt(100L),anyLong())).thenReturn("Успешно");
       when(mockDepositeServise.deposit(geq(100L),anyLong())).thenThrow(InsufficientFundsException.class);
   }
   @Test
    public void callIsUserAuthenticatedTest() throws InsufficientFundsException{
        controller.deposit(50L,100L);
        verify(mockAccount,times(1)).isUserAuthenticated((long) 100);

   }
   @Test
    public void successfulDeposit() throws InsufficientFundsException{
        controller.deposit(50L,100L);

   }
   @Test(expected = InsufficientFundsException.class)
    public void failedDeposit() throws InsufficientFundsException{
        controller.deposit(500L,100L);
   }

 
}
